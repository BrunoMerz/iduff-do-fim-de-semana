class User < ApplicationRecord
    require 'emailvalidation' # Custom Module para checar se um email é válido
    has_one :professor
    has_one :student
    
    validates :name,:password,:password_confirmation,:email, presence: true
    
    has_secure_password
    validates :password, length: {minimum:6}
    #validates :name,:enrollment,:email, uniqueness: true
    #####################################################
    
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i 
    validate :validemail ##Module custom para negar formatos q o validates_format_of não pega
    validate :uffmail ##Checka o domínio

    validate :cannot_be_admin
    has_many :entries

    def before_destroy
        if kind == "admin"
            errors.add(:cant_delete_admin, "O admin não pode ser deletado")
        end
    end

    def cannot_be_admin
        if kind == "admin"
            begin
                User.find_by(kind: "admin")
                errors.add(:only_one_admin_can_exist, "Não é possivel criar outro admin. Tente crair um Secretary")
            rescue => exception
                
            end
        end
    end

    after_create :create_kind_class

    enum kind: { ##Default: 1
        admin: 0,
        student: 1,
        professor: 2,
        secretary: 3
    }

    ##-----------------------Validate-Email----------------------#
    def validemail
        if Emailvalidation::valid(email) == false
            errors.add(:wrong_email_format, "Email com formatação Inválida")
        end
    end

    def uffmail ##Todos os emails que passarem por aqui tem o formato Username@Domain devido
                ##Ao method Validemail
        emailsplitted = email.split("@") ##Divide o email apartir de arrobas

        extension = emailsplitted[-1] ## Esperado id.uff.br, Pega a ultima parte do email passado
        template = "id.uff.br"
        if extension != template
            errors.add(:wrong_domain, "O Email não pertence ao domínio id.uff.br")
        end
    end
    ##-------------------------------------------------------------#
    def create_kind_class
        if kind == "student"
            Student.create(enrollment: id,user_id: id)
        elsif kind == "professor"
            Professor.create(enrollment: id,user_id: id)
        end
    end
end
