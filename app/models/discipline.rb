class Discipline < ApplicationRecord
    validates :name, :department, presence: true
    validates :name, uniqueness: true
    has_many :licenses
    has_many :classrooms
end
