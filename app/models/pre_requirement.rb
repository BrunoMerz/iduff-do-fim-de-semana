class PreRequirement < ApplicationRecord
  belongs_to :discipline
  validates :requirement_id ,presence: true
  validate :selfrequirement
  validate :duplicated
  validate :loop
  validate :requirement_exists

  def selfrequirement
    if discipline_id == requirement_id
      errors.add(:same_discipline, "Uma matéria não pode ser pré de sim mesma")
    end
  end

  def duplicated
    if PreRequirement.find_by(discipline_id: discipline_id,requirement_id: requirement_id)
      errors.add(:duplicate, "Pré requesito já existe")
    end
  end

  def loop
    if PreRequirement.find_by(discipline_id: requirement_id,requirement_id: discipline_id)
      errors.add(:pre_requirement_loop, "Matérias não podem ser pré requesitos entre si")
    end
  end
  
  def requirement_exists
    begin
      Discipline.find(requirement_id).present?
    rescue => exception
      errors.add(:pre_requirement_doesnt_exist, "Pré requesito não existe")
    end
  end
  
  
end
