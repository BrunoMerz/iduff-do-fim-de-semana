class License < ApplicationRecord
  belongs_to :professor
  belongs_to :discipline
  validate :uniquelicense

  def uniquelicense
    if License.find_by(professor_id: professor_id,discipline_id: discipline_id).present?
      errors.add(:duplicate, "Licensa já existe")
    end
  end

end
