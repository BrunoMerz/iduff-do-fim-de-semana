class Professor < ApplicationRecord
    belongs_to :user
    validates :enrollment, uniqueness: true
    validates :enrollment, presence: true
    has_many :licenses
    has_many :classrooms
end
