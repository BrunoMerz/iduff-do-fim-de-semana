class Student < ApplicationRecord
  belongs_to :user
  validates :enrollment, uniqueness: true
  validates :enrollment, presence: true
  has_many :entries
end
