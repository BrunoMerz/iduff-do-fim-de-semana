class Classroom < ApplicationRecord
  belongs_to :discipline
  belongs_to :days_of_the_week
  has_one :professor
  has_many :entries

  before_create :custom_deafult ##default pro status
  validate :days_binary_validation

  validates :initials,:schedule, presence: true
  validates :initials, uniqueness: true

  validate :cannot_not_have_professor ##Precisa de um professor enquanto estado de ativo

  validate :professor_must_exist

  def professor_must_exist
    unless professor_id == nil
      begin
        Professor.find(professor_id)
      rescue => exception
        errors.add(:professor_must_exist, "Professor deve existir")
      end
    end
  end
  

  def cannot_not_have_professor
    if status == 'active' || status == 'finished'
      if professor_id == nil
        errors.add(:must_have_professor, "Não é possível deixar a turma sem professor. Por favor, insira outro ao invés de null")
      end
    end    
  end
  

  enum status: { ##Default: 0
    closed: 0,  ##Incial
    being_built: 1, ##Aberto á alunos
    active: 2, ##Qunado posto um prof, fechado e funcional (Entries viram ativas tmb)
    finished: 3 ##Terminado qnd todas Entries foram finalizadas
}


  def days_binary_validation
    unless days_of_the_week_id == nil
      if days_of_the_week_id < 1 || days_of_the_week_id > 31
        errors.add(:days_of_the_week_out_of_range, "O número passado está fora do min ou max. (Min: 1,Max: 31)")
      end
    end
  end
  
  def custom_deafult
    self.status = 0
  end
  
end