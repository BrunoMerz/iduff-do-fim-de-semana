class Entry < ApplicationRecord
  belongs_to :classroom
  belongs_to :student
  validate :able_to_do

  after_save :finish_entry

  enum status: { ##Default: 0
    started: 0,
    finished: 1,
}

  def finish_entry
    if grade_one != nil && grade_two != nil && final_score == nil
      self.final_score = (grade_one + grade_two)/2
      self.status = 1
      CompletedDiscipline.create(student_id: student_id,discipline_id: Classroom.find(self.classroom_id).discipline_id)
      self.save
    end
  end

  def able_to_do
    pre = PreRequirement.where(discipline_id: Classroom.find(self.classroom_id).discipline_id)
    for x in 0 .. pre.length - 1
      if CompletedDiscipline.find_by(student_id: student_id,discipline_id: pre[x]['requirement_id']).nil?
        errors.add(:pre_requirement_not_met, "O pré requesito não foi alcançado pelo estudante")
      end
    end
  end
  
  
end
