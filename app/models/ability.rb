# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    
    if user
      can :create, Entry
    end

    if user.admin?
      can :manage, :all
    end

    if user.secretary?
      can :manage, :all
    end

  end
end
