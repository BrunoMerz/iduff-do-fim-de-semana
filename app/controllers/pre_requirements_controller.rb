class PreRequirementsController < ApplicationController
  before_action :set_pre_requirement, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource

  # GET /pre_requirements
  def index
    @pre_requirements = PreRequirement.all

    render json: @pre_requirements
  end

  # GET /pre_requirements/1
  def show
    render json: @pre_requirement
  end

  # POST /pre_requirements
  def create
    @pre_requirement = PreRequirement.new(pre_requirement_params)

    if @pre_requirement.save
      render json: @pre_requirement, status: :created, location: @pre_requirement
    else
      render json: @pre_requirement.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pre_requirements/1
  def update
    if @pre_requirement.update(pre_requirement_params)
      render json: @pre_requirement
    else
      render json: @pre_requirement.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pre_requirements/1
  def destroy
    @pre_requirement.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pre_requirement
      @pre_requirement = PreRequirement.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pre_requirement_params
      params.require(:pre_requirement).permit(:discipline_id, :requirement_id)
    end
end
