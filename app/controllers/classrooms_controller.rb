class ClassroomsController < ApplicationController
  before_action :set_classroom, only: [:show, :update, :destroy]
  before_action :must_be_signed_in
  load_and_authorize_resource

  # GET /classrooms
  def index
    @classrooms = Classroom.all

    render json: @classrooms
  end

  # GET /classrooms/1
  def show
    render json: @classroom
  end

  # POST /classrooms
  def create
    @classroom = Classroom.new(classroom_params)

    if @classroom.save
      render json: @classroom, status: :created, location: @classroom
    else
      render json: @classroom.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /classrooms/1
  def update
    if @classroom.update(classroom_put_patch_params)
      render json: @classroom
    else
      render json: @classroom.errors, status: :unprocessable_entity
    end
  end

  # DELETE /classrooms/1
  def destroy
    @classroom.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classroom
      @classroom = Classroom.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def classroom_params
      params.require(:classroom).permit(:initials, :days_of_the_week_id, :schedule,:discipline_id, :professor_id)
    end
    def classroom_put_patch_params
      params.require(:classroom).permit(:days_of_the_week_id, :schedule, :status, :professor_id)
    end
end
