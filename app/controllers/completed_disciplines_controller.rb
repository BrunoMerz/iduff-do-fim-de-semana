class CompletedDisciplinesController < ApplicationController
  before_action :set_completed_discipline, only: [:show, :update, :destroy]

  # GET /completed_disciplines
  def index
    @completed_disciplines = CompletedDiscipline.all

    render json: @completed_disciplines
  end

  # GET /completed_disciplines/1
  def show
    render json: @completed_discipline
  end

  # POST /completed_disciplines
  def create
    @completed_discipline = CompletedDiscipline.new(completed_discipline_params)

    if @completed_discipline.save
      render json: @completed_discipline, status: :created, location: @completed_discipline
    else
      render json: @completed_discipline.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /completed_disciplines/1
  def update
    if @completed_discipline.update(completed_discipline_params)
      render json: @completed_discipline
    else
      render json: @completed_discipline.errors, status: :unprocessable_entity
    end
  end

  # DELETE /completed_disciplines/1
  def destroy
    @completed_discipline.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_completed_discipline
      @completed_discipline = CompletedDiscipline.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def completed_discipline_params
      params.require(:completed_discipline).permit(:student_id, :discipline_id)
    end
end
