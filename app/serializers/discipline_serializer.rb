class DisciplineSerializer < ActiveModel::Serializer
  attributes :id, :name, :department
  has_many :licenses
  has_many :classrooms

end
