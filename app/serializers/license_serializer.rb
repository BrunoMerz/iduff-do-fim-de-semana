class LicenseSerializer < ActiveModel::Serializer
  attributes :id
  has_one :professor
  has_one :discipline
end
