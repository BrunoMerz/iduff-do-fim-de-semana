class ProfessorSerializer < ActiveModel::Serializer
  attributes :id, :enrollment
  has_one :user
  has_many :classrooms
end
