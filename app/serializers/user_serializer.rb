class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :kind
  has_one :professor, if: :showprof
  has_one :student, if: :showstudent
  
  def showprof
    object.professor
  end

  def showstudent
    object.student
  end
end
