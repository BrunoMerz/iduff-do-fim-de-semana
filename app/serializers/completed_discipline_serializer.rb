class CompletedDisciplineSerializer < ActiveModel::Serializer
  attributes :id
  has_one :student
  has_one :discipline
end
