class StudentSerializer < ActiveModel::Serializer
  attributes :id, :enrollment, :cr
  has_one :user
  attribute :possible_classes

  def possible_classes
    a = Classroom.where(status: 1)
    bas = {}
    for x in 0 .. a.length - 1
      temp = returnhash(a[x])
      bas.store(a[x]["initials"] , temp)
    end
    return bas
  end

  def returnhash(array)
    return {
      "classroom_id": array["id"],
      "discipline_name": Discipline.find(array["discipline_id"]).name,
      "department": Discipline.find(array["discipline_id"]).department
    }
  end
  
  
  has_many :entries
end
