class EntrySerializer < ActiveModel::Serializer
  attributes :id, :final_score, :grade_one, :grade_two, :status
  has_one :classroom
  has_one :student
end
