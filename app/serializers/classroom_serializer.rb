class ClassroomSerializer < ActiveModel::Serializer
  attributes :id, :initials, :schedule, :status
  belongs_to :discipline
  attribute :days
  has_one :professor
  has_many :entries
  
  def days
    days_table = DaysOfTheWeek.find(object.days_of_the_week_id)
    return {
      'monday': days_table['monday'],
      'tuesday': days_table['tuesday'],
      'wednesday': days_table['wednesday'],
      'thursday': days_table['thursday'],
      'friday': days_table['friday']
    }
  end

  
end
