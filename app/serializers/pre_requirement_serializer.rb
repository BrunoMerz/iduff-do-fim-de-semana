class PreRequirementSerializer < ActiveModel::Serializer
  attributes :id
  has_one :discipline
  has_one :requirement

  def requirement
    disc = Discipline.find(object.requirement_id).attributes()
    return {
      'id': disc['id'],
      'name': disc['name'],
      'department': disc['department']
    }
  end
  
end
