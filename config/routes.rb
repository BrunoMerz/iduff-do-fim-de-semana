Rails.application.routes.draw do

  resources :completed_disciplines
  resources :entries
  #resources :days_of_the_weeks ## Não deve estar valido em momento algum
  
  resources :classrooms
  resources :pre_requirements
  resources :licenses
  resources :disciplines
  #get 'session/login'
  #resources :students ## Não deve estar valido em momento algum
  #resources :professors ## Não deve estar valido em momento algum

  post '/login', to: 'session#login'
  get '/current_user', to: 'application#user_must_exist'
  resources :users, expect:[:destroy, :update]                               

  get '/students', to: 'students#index'
  get '/professors', to: 'professors#index'
  post '/register', to: 'users#create'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
