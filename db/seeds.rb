# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: "admin",email: "admin@id.uff.br",password:"123456",
password_confirmation:"123456",kind: 0)

DaysOfTheWeek.destroy_all
for binary in 1..31
    days = [false,false,false,false,false]
    if binary >= 16                        
      days[4] = true                       
      binary -= 16
    end
    if binary >= 8
      days[3] = true
      binary -= 8
    end
    if binary >= 4
      days[2] = true
      binary -= 4
    end
    if binary >= 2
      days[1] = true
      binary -= 2
    end
    if binary >= 1
      days[0] = true
      binary -= 1
    end
    a = DaysOfTheWeek.create(
      "monday": days[0],
      "tuesday": days[1],
      "wednesday": days[2],
      "thursday": days[3],
      "friday": days[4]
    )
end