class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.references :user, foreign_key: true
      t.string :enrollment
      t.float :cr, default: 0

      t.timestamps
    end
  end
end
