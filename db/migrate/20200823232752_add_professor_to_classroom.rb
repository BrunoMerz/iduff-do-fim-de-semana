class AddProfessorToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :professor, foreign_key: true
  end
end
