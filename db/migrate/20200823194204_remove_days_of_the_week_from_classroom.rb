class RemoveDaysOfTheWeekFromClassroom < ActiveRecord::Migration[5.2]
  def change
    remove_reference :classrooms, :days_of_the_week, foreign_key: true
  end
end
