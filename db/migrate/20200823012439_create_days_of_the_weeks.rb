class CreateDaysOfTheWeeks < ActiveRecord::Migration[5.2]
  def change
    create_table :days_of_the_weeks do |t|
      t.references :class, foreign_key: true
      t.boolean :monday, default: false
      t.boolean :tuesday, default: false
      t.boolean :wednesday, default: false
      t.boolean :thursday, default: false
      t.boolean :friday, default: false

      t.timestamps
    end
  end
end
