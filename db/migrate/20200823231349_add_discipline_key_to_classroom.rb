class AddDisciplineKeyToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :discipline, foreign_key: true
  end
end
