class RemoveIntegerFromClassroom < ActiveRecord::Migration[5.2]
  def change
    remove_column :classrooms, :days_of_the_week_id, :integer
  end
end
