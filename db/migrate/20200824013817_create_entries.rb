class CreateEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :entries do |t|
      t.float :final_score
      t.float :grade_one
      t.float :grade_two
      t.integer :status , default: 0
      t.references :classroom, foreign_key: true
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
