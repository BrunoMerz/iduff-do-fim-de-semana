class CreatePreRequirements < ActiveRecord::Migration[5.2]
  def change
    create_table :pre_requirements do |t|
      t.references :discipline, foreign_key: true
      t.integer :requirement_id

      t.timestamps
    end
  end
end
