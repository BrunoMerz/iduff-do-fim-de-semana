class CreateLicenses < ActiveRecord::Migration[5.2]
  def change
    create_table :licenses do |t|
      t.references :professor, foreign_key: true
      t.references :discipline, foreign_key: true

      t.timestamps
    end
  end
end
