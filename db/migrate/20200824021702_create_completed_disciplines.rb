class CreateCompletedDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_disciplines do |t|
      t.references :student, foreign_key: true
      t.references :discipline, foreign_key: true

      t.timestamps
    end
  end
end
