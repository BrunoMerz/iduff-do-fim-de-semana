class RemoveEnrollmenttFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :enrollment, :string
  end
end
