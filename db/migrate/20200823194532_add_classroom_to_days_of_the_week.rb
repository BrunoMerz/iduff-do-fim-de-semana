class AddClassroomToDaysOfTheWeek < ActiveRecord::Migration[5.2]
  def change
    add_reference :days_of_the_weeks, :classroom, foreign_key: true
  end
end
