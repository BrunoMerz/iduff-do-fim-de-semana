class AddDaysOfTheWeekToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_column :classrooms, :days_of_the_week, :integer
  end
end
