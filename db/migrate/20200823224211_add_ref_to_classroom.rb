class AddRefToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :days_of_the_week, foreign_key: true
  end
end
