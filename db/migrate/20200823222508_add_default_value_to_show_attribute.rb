class AddDefaultValueToShowAttribute < ActiveRecord::Migration[5.2]
  def up
    change_column :classrooms, :status, :integer, default: 0
  end
  
  def down
    change_column :classrooms, :status, :integer, default: nil
  end
end
