class RemoveClassFromDaysOfTheWeek < ActiveRecord::Migration[5.2]
  def change
    remove_reference :days_of_the_weeks, :class, foreign_key: true
  end
end
