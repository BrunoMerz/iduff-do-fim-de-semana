class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :initials
      t.references :days_of_the_week
      t.string :schedule
      t.integer :status

      t.timestamps
    end
  end
end
