class RemoveClassroomFromDaysOfTheWeek < ActiveRecord::Migration[5.2]
  def change
    remove_reference :days_of_the_weeks, :classroom, foreign_key: true
  end
end
