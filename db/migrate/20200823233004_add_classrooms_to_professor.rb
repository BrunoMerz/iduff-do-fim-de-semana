class AddClassroomsToProfessor < ActiveRecord::Migration[5.2]
  def change
    add_reference :professors, :classroom, foreign_key: true
  end
end
