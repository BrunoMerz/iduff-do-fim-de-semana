# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_24_021702) do

  create_table "classrooms", force: :cascade do |t|
    t.string "initials"
    t.string "schedule"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "days_of_the_week_id"
    t.integer "discipline_id"
    t.integer "professor_id"
    t.index ["days_of_the_week_id"], name: "index_classrooms_on_days_of_the_week_id"
    t.index ["discipline_id"], name: "index_classrooms_on_discipline_id"
    t.index ["professor_id"], name: "index_classrooms_on_professor_id"
  end

  create_table "completed_disciplines", force: :cascade do |t|
    t.integer "student_id"
    t.integer "discipline_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_completed_disciplines_on_discipline_id"
    t.index ["student_id"], name: "index_completed_disciplines_on_student_id"
  end

  create_table "days_of_the_weeks", force: :cascade do |t|
    t.boolean "monday"
    t.boolean "tuesday"
    t.boolean "wednesday"
    t.boolean "thursday"
    t.boolean "friday"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", force: :cascade do |t|
    t.string "name"
    t.string "department"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entries", force: :cascade do |t|
    t.float "final_score"
    t.float "grade_one"
    t.float "grade_two"
    t.integer "status", default: 0
    t.integer "classroom_id"
    t.integer "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classroom_id"], name: "index_entries_on_classroom_id"
    t.index ["student_id"], name: "index_entries_on_student_id"
  end

  create_table "licenses", force: :cascade do |t|
    t.integer "professor_id"
    t.integer "discipline_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_licenses_on_discipline_id"
    t.index ["professor_id"], name: "index_licenses_on_professor_id"
  end

  create_table "pre_requirements", force: :cascade do |t|
    t.integer "discipline_id"
    t.integer "requirement_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_pre_requirements_on_discipline_id"
  end

  create_table "professors", force: :cascade do |t|
    t.string "enrollment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "classroom_id"
    t.index ["classroom_id"], name: "index_professors_on_classroom_id"
    t.index ["user_id"], name: "index_professors_on_user_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "user_id"
    t.string "enrollment"
    t.float "cr", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_students_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "kind", default: 1
  end

end
