require 'test_helper'

class PreRequirementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pre_requirement = pre_requirements(:one)
  end

  test "should get index" do
    get pre_requirements_url, as: :json
    assert_response :success
  end

  test "should create pre_requirement" do
    assert_difference('PreRequirement.count') do
      post pre_requirements_url, params: { pre_requirement: { discipline_id: @pre_requirement.discipline_id, requirement_id: @pre_requirement.requirement_id } }, as: :json
    end

    assert_response 201
  end

  test "should show pre_requirement" do
    get pre_requirement_url(@pre_requirement), as: :json
    assert_response :success
  end

  test "should update pre_requirement" do
    patch pre_requirement_url(@pre_requirement), params: { pre_requirement: { discipline_id: @pre_requirement.discipline_id, requirement_id: @pre_requirement.requirement_id } }, as: :json
    assert_response 200
  end

  test "should destroy pre_requirement" do
    assert_difference('PreRequirement.count', -1) do
      delete pre_requirement_url(@pre_requirement), as: :json
    end

    assert_response 204
  end
end
