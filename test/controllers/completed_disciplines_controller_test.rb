require 'test_helper'

class CompletedDisciplinesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @completed_discipline = completed_disciplines(:one)
  end

  test "should get index" do
    get completed_disciplines_url, as: :json
    assert_response :success
  end

  test "should create completed_discipline" do
    assert_difference('CompletedDiscipline.count') do
      post completed_disciplines_url, params: { completed_discipline: { discipline_id: @completed_discipline.discipline_id, student_id: @completed_discipline.student_id } }, as: :json
    end

    assert_response 201
  end

  test "should show completed_discipline" do
    get completed_discipline_url(@completed_discipline), as: :json
    assert_response :success
  end

  test "should update completed_discipline" do
    patch completed_discipline_url(@completed_discipline), params: { completed_discipline: { discipline_id: @completed_discipline.discipline_id, student_id: @completed_discipline.student_id } }, as: :json
    assert_response 200
  end

  test "should destroy completed_discipline" do
    assert_difference('CompletedDiscipline.count', -1) do
      delete completed_discipline_url(@completed_discipline), as: :json
    end

    assert_response 204
  end
end
