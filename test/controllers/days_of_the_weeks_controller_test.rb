require 'test_helper'

class DaysOfTheWeeksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @days_of_the_week = days_of_the_weeks(:one)
  end

  test "should get index" do
    get days_of_the_weeks_url, as: :json
    assert_response :success
  end

  test "should create days_of_the_week" do
    assert_difference('DaysOfTheWeek.count') do
      post days_of_the_weeks_url, params: { days_of_the_week: { class_id: @days_of_the_week.class_id, friday: @days_of_the_week.friday, monday: @days_of_the_week.monday, thursday: @days_of_the_week.thursday, tuesday: @days_of_the_week.tuesday, wednesday: @days_of_the_week.wednesday } }, as: :json
    end

    assert_response 201
  end

  test "should show days_of_the_week" do
    get days_of_the_week_url(@days_of_the_week), as: :json
    assert_response :success
  end

  test "should update days_of_the_week" do
    patch days_of_the_week_url(@days_of_the_week), params: { days_of_the_week: { class_id: @days_of_the_week.class_id, friday: @days_of_the_week.friday, monday: @days_of_the_week.monday, thursday: @days_of_the_week.thursday, tuesday: @days_of_the_week.tuesday, wednesday: @days_of_the_week.wednesday } }, as: :json
    assert_response 200
  end

  test "should destroy days_of_the_week" do
    assert_difference('DaysOfTheWeek.count', -1) do
      delete days_of_the_week_url(@days_of_the_week), as: :json
    end

    assert_response 204
  end
end
