module Emailvalidation
    def self.valid(email)
        emailsplitted_at = email.split("@") ## Separa o Username do Domnínio

        if emailsplitted_at.length == 1
            return false

        elsif emailsplitted_at.length > 2     ##Esses 3 checkam se ele tem o formato User@Domain
            return false
        
        elsif emailsplitted_at[0] == ""
            return false
        end

        emailsplitted_period = emailsplitted_at[0].split('.')
        num_of_periods = 0
        iterated = emailsplitted_at[0].split('') 

        if iterated[0] == "." || iterated[-1] == '.'
            return false
        end

        cycle = 0
        for x in iterated
            if x == "."
                num_of_periods += 1
            end
            if x == "." && iterated[cycle + 1] == "."   ##Não é possivel ter um user com a..a@
                return false
            end
            cycle += 1
        end
        if emailsplitted_period.length != (num_of_periods + 1) ##Ter certeza do número de pontos
            return false
        end
    end
end